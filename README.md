# Roadmap

## Core features

- [ ] All-to-all messaging protocol
- [ ] Authentication ?
  - [ ] User registration
  - [ ] User login

## Additional features

- [ ] Channels
  - [ ] Create Channel
  - [ ] Join Channel
  - [ ] Leave Channel
  - [ ] List Channels
  - [ ] List Users in Channel
  - [ ] Direct Messages
- [ ] Persistent messages ?
